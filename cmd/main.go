package main

import "gitlab.com/go_mc_server/go_master_classes_back/internal/pkg"

func main() {
	pkg.StartApplication()
}
