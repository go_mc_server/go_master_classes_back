CREATE TYPE TypeNeedlework AS ENUM (
    'Crocheting',
    'KnittingWithNeedles',
    'Macrame',
    'Embroidery',
    'Sewing',
    'Painting',
    'Felting',
    'Modeling'
);

CREATE TABLE master_classes (
    id SERIAL PRIMARY KEY,
    author_id INTEGER REFERENCES users(id) ON DELETE CASCADE NOT NULL,
    type_needlework TypeNeedlework NOT NULL,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(500),
    materials TEXT,
    detail_info TEXT,
    is_active BOOLEAN DEFAULT true,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);