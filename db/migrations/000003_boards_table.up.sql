CREATE TYPE CurrentStatus AS ENUM (
    'Idea',
    'ToDo',
    'InProgress',
    'Done',
    'Archived'
);

CREATE TABLE boards (
     id SERIAL PRIMARY KEY,
     user_id INTEGER REFERENCES users(id) ON DELETE CASCADE NOT NULL,
     master_class_id INTEGER REFERENCES master_classes(id) ON DELETE CASCADE NOT NULL,
     current_status CurrentStatus DEFAULT 'Idea',
     note TEXT,
     created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
     updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
     deleted_at TIMESTAMP WITH TIME ZONE
);