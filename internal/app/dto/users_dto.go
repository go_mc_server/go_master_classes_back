package dto

import (
	"time"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
)

type UserDTO struct {
	ID         uint            `json:"id"`
	LastName   string          `json:"lastName"`
	FirstName  string          `json:"firstName"`
	MiddleName string          `json:"middleName"`
	Email      string          `json:"email"`
	Role       models.RoleType `json:"role"`
	CreatedAt  time.Time       `json:"createdAt"`
	UpdatedAt  time.Time       `json:"updatedAt"`
}

type UserDetailDTO struct {
	ID         uint            `json:"id"`
	LastName   string          `json:"lastName"`
	FirstName  string          `json:"firstName"`
	MiddleName string          `json:"middleName"`
	Email      string          `json:"email"`
	Password   string          `json:"password"`
	Role       models.RoleType `json:"role"`
	CreatedAt  time.Time       `json:"createdAt"`
	UpdatedAt  time.Time       `json:"updatedAt"`
}
