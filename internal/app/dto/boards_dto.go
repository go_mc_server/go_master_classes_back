package dto

import (
	"time"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
)

type BoardDetailDTO struct {
	ID            uint64               `json:"id"`
	UserID        uint64               `json:"user_id"`
	MasterClass   MasterClassDTO       `json:"master_class"`
	CurrentStatus models.CurrentStatus `json:"current_status"`
	Note          string               `json:"notes"`
	CreatedAt     time.Time            `json:"createdAt"`
	UpdatedAt     time.Time            `json:"updatedAt"`
}

type BoardDTO struct {
	ID            uint64               `json:"id"`
	UserID        uint64               `json:"user_id"`
	MasterClassID uint64               `json:"master_class_id"`
	CurrentStatus models.CurrentStatus `json:"current_status"`
	Note          string               `json:"note"`
	IsActive      bool                 `json:"is_active"`
	CreatedAt     time.Time            `json:"createdAt"`
	UpdatedAt     time.Time            `json:"updatedAt"`
}
