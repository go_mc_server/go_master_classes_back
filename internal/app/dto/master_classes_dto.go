package dto

import (
	"time"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
)

type MasterClassDTO struct {
	ID             uint64                `json:"id"`
	AuthorID       uint64                `json:"author_id"`
	TypeNeedlework models.TypeNeedlework `json:"type_needlework"`
	Name           string                `json:"name"`
	Description    string                `json:"description"`
	Materials      string                `json:"materials"`
	DetailInfo     string                `json:"detail_info"`
	IsActive       bool                  `json:"is_active"`
	CreatedAt      time.Time             `json:"createdAt"`
	UpdatedAt      time.Time             `json:"updatedAt"`
}

type MasterClassDetailDTO struct {
	ID             uint64                `json:"id"`
	Author         UserDTO               `json:"author_id"`
	TypeNeedlework models.TypeNeedlework `json:"type_needlework"`
	Name           string                `json:"name"`
	Description    string                `json:"description"`
	Materials      string                `json:"materials"`
	DetailInfo     string                `json:"detail_info"`
	IsActive       bool                  `json:"is_active"`
	CreatedAt      time.Time             `json:"createdAt"`
	UpdatedAt      time.Time             `json:"updatedAt"`
}
