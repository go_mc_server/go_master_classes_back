package repositories

import (
	"context"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
)

type IUsersRepository interface {
	FindAll(ctx context.Context, filterUser *filtering.UsersFilter) (*[]models.Users, error)
	FindByID(ctx context.Context, id uint) (*models.Users, error)
	FindByEmail(ctx context.Context, email string) (*models.Users, error)
	Create(ctx context.Context, user *models.Users) error
	Update(ctx context.Context, updatedUser *models.Users) (*models.Users, error)
	UpdatePassword(ctx context.Context, updatedUser *models.Users) (*models.Users, error)
	Delete(ctx context.Context, id uint) error
}

type IMasterClassesRepository interface {
	FindAll(ctx context.Context, filterMC *filtering.MasterClassesFilter) (*[]models.MasterClasses, error)
	FindByID(ctx context.Context, id uint) (*models.MasterClasses, error)
	Create(ctx context.Context, masterClass *models.MasterClasses) error
	UpdateUserCreateMC(ctx context.Context, masterClass *models.MasterClasses) error
	Update(ctx context.Context, updatedMC *models.MasterClasses) (*models.MasterClasses, error)
	Delete(ctx context.Context, authorID, masterClassID uint) error
}

type IBoardsRepository interface {
	FindAll(ctx context.Context, filterBoards *filtering.BoardsFilter) (*[]models.Boards, error)
	FindByID(ctx context.Context, id uint) (*models.Boards, error)
	Create(ctx context.Context, board *models.Boards) error
	Update(ctx context.Context, updatedBoard *models.Boards) (*models.Boards, error)
	Delete(ctx context.Context, userID, boardID uint) error
}
