package repositories

import (
	"context"
	"errors"

	"gorm.io/gorm"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
)

type BoardsRepository struct {
	DB *gorm.DB
}

func (r *BoardsRepository) FindAll(
	ctx context.Context, filterBoards *filtering.BoardsFilter) (*[]models.Boards, error) {
	var boards []models.Boards
	query := r.DB.WithContext(ctx)
	if filterBoards.UserID != 0 {
		query = query.Where("user_id = ?", filterBoards.UserID)
	}
	if filterBoards.MasterClassID != 0 {
		query = query.Where("master_class_id = ?", filterBoards.MasterClassID)
	}
	if filterBoards.CurrentStatus != "" {
		query = query.Where("current_status = ?", filterBoards.CurrentStatus)
	}
	err := query.Find(&boards).Error
	return &boards, err
}

func (r *BoardsRepository) FindByID(
	ctx context.Context, id uint) (*models.Boards, error) {
	var board models.Boards
	err := r.DB.WithContext(ctx).
		Preload("MasterClass").
		First(&board, id).
		Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, mistakes.ErrNotFound
		}
		return nil, err
	}
	return &board, nil
}

func (r *BoardsRepository) Create(
	ctx context.Context, board *models.Boards) error {
	var masterClass models.MasterClasses
	err := r.DB.WithContext(ctx).
		First(&masterClass, board.MasterClassID).
		Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return mistakes.ErrNotFound
		}
		return err
	}
	return r.DB.Create(board).Error
}

func (r *BoardsRepository) Update(
	ctx context.Context, updateBoard *models.Boards) (*models.Boards, error) {
	var board models.Boards
	result := r.DB.WithContext(ctx).
		Model(&board).
		Where("id = ?", updateBoard.ID).
		Where("user_id = ?", updateBoard.UserID).
		Updates(updateBoard)
	if result.Error != nil {
		return nil, result.Error
	}
	if result.RowsAffected == 0 {
		return nil, mistakes.ErrNotFound
	}
	if err := r.DB.WithContext(ctx).
		First(&board, updateBoard.ID).
		Error; err != nil {
		return nil, err
	}
	return &board, nil
}

func (r *BoardsRepository) Delete(
	ctx context.Context, userID, boardID uint) error {
	result := r.DB.WithContext(ctx).
		Where("id = ? AND user_id = ?", boardID, userID).
		Delete(&models.Boards{})

	if result.Error != nil {
		return result.Error
	}
	// Проверяем, была ли удалена запись
	if result.RowsAffected == 0 {
		return errors.New("record not found or does not belong to the user")
	}

	return nil
}

// Подтвердждаем, что BoardsRepository реализует интерфейс IBoardsRepository
var _ IBoardsRepository = (*BoardsRepository)(nil)
