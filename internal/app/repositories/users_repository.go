// Логика взаимодействия с базой данных или другими хранилищами данных для сущности пользователя
package repositories

import (
	"context"
	"errors"

	"gorm.io/gorm"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
)

type UsersRepository struct {
	DB *gorm.DB
}

func (r *UsersRepository) FindAll(
	ctx context.Context, filterUser *filtering.UsersFilter) (*[]models.Users, error) {
	var users []models.Users
	query := r.DB.WithContext(ctx)

	if filterUser.LastName != "" {
		query = query.Where("last_name ILIKE ?", "%"+filterUser.LastName+"%")
	}
	if filterUser.FirstName != "" {
		query = query.Where("first_name ILIKE ?", "%"+filterUser.FirstName+"%")
	}
	if filterUser.MiddleName != "" {
		query = query.Where("middle_name ILIKE ?", "%"+filterUser.MiddleName+"%")
	}
	if filterUser.Email != "" {
		query = query.Where("email ILIKE ?", "%"+filterUser.Email+"%")
	}
	if filterUser.Role != "" {
		query = query.Where("role = ?", filterUser.Role)
	}

	err := query.Find(&users).Error
	if err != nil {
		return nil, err
	}

	return &users, nil
}

func (r *UsersRepository) FindByID(
	ctx context.Context, id uint) (*models.Users, error) {
	var user models.Users
	err := r.DB.WithContext(ctx).First(&user, id).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, mistakes.ErrNotFound
		}
		return nil, err
	}
	return &user, nil
}

func (r *UsersRepository) FindByEmail(
	ctx context.Context, email string) (*models.Users, error) {
	var user models.Users
	err := r.DB.WithContext(ctx).First(&user, "email = ?", email).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *UsersRepository) Create(
	ctx context.Context, user *models.Users) error {
	return r.DB.WithContext(ctx).Create(user).Error
}

func (r *UsersRepository) Update(
	ctx context.Context, updatedUser *models.Users) (*models.Users, error) {
	var user models.Users
	err := r.DB.WithContext(ctx).
		Model(&user).
		Where("id = ?", updatedUser.ID).
		Updates(updatedUser).
		Error
	if err != nil {
		return nil, err
	}
	if err = r.DB.WithContext(ctx).
		First(&user, updatedUser.ID).
		Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *UsersRepository) UpdatePassword(
	ctx context.Context, updatedUser *models.Users) (*models.Users, error) {
	var user models.Users
	err := r.DB.WithContext(ctx).
		Model(&user).
		Where("email = ?", updatedUser.Email).
		Where("last_name ILIKE ?", updatedUser.LastName).
		Where("first_name ILIKE ?", updatedUser.FirstName).
		Update("password", updatedUser.Password).
		Error
	if err != nil {
		return nil, err
	}
	if err = r.DB.WithContext(ctx).
		First(&user, "email = ?", updatedUser.Email).
		Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *UsersRepository) Delete(
	ctx context.Context, id uint) error {
	//жёсткое удаление (физическое удаление записи)
	return r.DB.WithContext(ctx).Unscoped().Delete(&models.Users{}, id).Error
}

// Подтвердждаем, что UsersRepository реализует интерфейс IUsersRepository
var _ IUsersRepository = (*UsersRepository)(nil)
