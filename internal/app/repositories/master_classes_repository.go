package repositories

import (
	"context"
	"errors"
	"fmt"

	"gorm.io/gorm"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
)

type MasterClassesRepository struct {
	DB *gorm.DB
}

func (r *MasterClassesRepository) FindAll(
	ctx context.Context, filterMC *filtering.MasterClassesFilter) (*[]models.MasterClasses, error) {
	var masterClasses []models.MasterClasses
	query := r.DB.WithContext(ctx)
	if filterMC.AuthorID != 0 {
		query = query.Where("author_id = ?", filterMC.AuthorID)
	}
	if filterMC.TypeNeedlework != "" {
		query = query.Where("type_needlework = ?", filterMC.TypeNeedlework)
	}
	if filterMC.Name != "" {
		query = query.Where("name ILIKE ?", "%"+filterMC.Name+"%")
	}
	if filterMC.Materials != "" {
		query = query.Where("materials ILIKE ?", "%"+filterMC.Materials+"%")
	}
	if filterMC.IsActive != nil {
		query = query.Where("is_active = ?", filterMC.IsActive)
	}
	//err := query.Debug().Find(&masterClasses).Error
	err := query.Find(&masterClasses).Error
	if err != nil {
		return nil, err
	}
	return &masterClasses, err
}

func (r *MasterClassesRepository) FindByID(
	ctx context.Context, id uint) (*models.MasterClasses, error) {
	var masterClass models.MasterClasses
	err := r.DB.WithContext(ctx).
		Preload("Author").
		First(&masterClass, id).
		Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, mistakes.ErrNotFound
		}
		return nil, err
	}
	return &masterClass, nil
}

func (r *MasterClassesRepository) Create(
	ctx context.Context, masterClass *models.MasterClasses) error {
	return r.DB.WithContext(ctx).Create(masterClass).Error
}

func (r *MasterClassesRepository) UpdateUserCreateMC(
	ctx context.Context, masterClass *models.MasterClasses) error {
	// Начало транзакции
	tx := r.DB.WithContext(ctx).Begin()
	if tx.Error != nil {
		return fmt.Errorf("failed to start transaction: %w", tx.Error)
	}

	// Обеспечение завершения транзакции в случае паники
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	//обновление пользователя
	var user models.Users
	result := tx.Model(&user).Where("id = ?", masterClass.AuthorID).
		Update("role", models.Master)
	if result.Error != nil {
		tx.Rollback()
		return fmt.Errorf("error while updating user role: %w", result.Error)
	}

	// Проверка количества строк, затронутых обновлением.
	//Если 0 то пользователь был не найден.
	if result.RowsAffected == 0 {
		tx.Rollback()
		return fmt.Errorf("no user found with ID %d", masterClass.AuthorID)
	}

	//создание мастер-класса
	if err := tx.Create(masterClass).Error; err != nil {
		tx.Rollback()
		return fmt.Errorf("error while creating masterClass: %w", err)
	}

	//завершение транзакции
	if err := tx.Commit().Error; err != nil {
		return fmt.Errorf("failed to commit transaction: %w", err)
	}

	return nil
}

func (r *MasterClassesRepository) Update(
	ctx context.Context, updatedMC *models.MasterClasses) (*models.MasterClasses, error) {
	var masterClass models.MasterClasses
	result := r.DB.WithContext(ctx).
		Model(&masterClass).
		Where("id = ?", updatedMC.ID).
		Where("author_id = ?", updatedMC.AuthorID).
		Updates(updatedMC)
	if result.Error != nil {
		return nil, result.Error
	}
	if result.RowsAffected == 0 {
		return nil, mistakes.ErrNotFound
	}
	if err := r.DB.WithContext(ctx).
		First(&masterClass, updatedMC.ID).
		Error; err != nil {
		return nil, err
	}

	return &masterClass, nil
}

func (r *MasterClassesRepository) Delete(
	ctx context.Context, authorID, masterClassID uint) error {
	// Мягкое удаление (отмечается время удаления записи в бд)
	//return r.DB.Delete(&models.MasterClasses{}, masterClassID).Error
	result := r.DB.WithContext(ctx).
		Where("id = ? AND author_id = ?", masterClassID, authorID).
		Delete(&models.MasterClasses{})

	if result.Error != nil {
		return result.Error
	}
	// Проверяем, была ли удалена запись
	if result.RowsAffected == 0 {
		return errors.New("record not found or does not belong to the author")
	}

	return nil
}

// Подтвердждаем, что MasterClassesRepository реализует интерфейс IMasterClassesRepository
var _ IMasterClassesRepository = (*MasterClassesRepository)(nil)
