package controllers

import (
	"context"

	pbBoard "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/boards"
	pbMC "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/master_classes"
	pbUser "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
)

type IUsersController interface {
	GetUsers(ctx context.Context, filterU *pbUser.FilterUsers) ([]*pbUser.UsersDTO, error)
	GetUserByID(ctx context.Context, id uint) (*pbUser.UserDTO, error)
	LoginUser(ctx context.Context, email, password string) (*pbUser.UserDTO, error)
	CreateUser(ctx context.Context, createUser *pbUser.CreateUserDTO) (*pbUser.NewUserDTO, error)
	UpdateUser(ctx context.Context, updateUser *pbUser.UpdateUserDTO) (*pbUser.NewUserDTO, error)
	UpdatePassword(ctx context.Context, updatePassword *pbUser.UpdatePasswordDTO) (*pbUser.NewUserDTO, error)
	DeleteUser(ctx context.Context, id uint) (*pbUser.Empty, error)
}

type IMasterClassesController interface {
	GetMasterClasses(ctx context.Context, filterMC *pbMC.FilterMasterClasses) ([]*pbMC.MasterClassesDTO, error)
	GetMasterClassByID(ctx context.Context, id uint) (*pbMC.MasterClassDTO, error)
	CreateMasterClass(ctx context.Context, createMC *pbMC.CreateMasterClassDTO) (*pbMC.NewMasterClassDTO, error)
	UpdateUserCreateMC(ctx context.Context, updateMC *pbMC.CreateMasterClassDTO) (*pbMC.NewMasterClassDTO, error)
	UpdateMasterClass(ctx context.Context, updateMC *pbMC.UpdateMasterClassDTO) (*pbMC.NewMasterClassDTO, error)
	DeleteMasterClass(ctx context.Context, authorID, masterClassID uint) (*pbMC.Empty, error)
}

type IBoardsController interface {
	GetBoards(ctx context.Context, filterBoards *pbBoard.FilterBoards) ([]*pbBoard.BoardsDTO, error)
	GetBoardByID(ctx context.Context, id uint) (*pbBoard.BoardDTO, error)
	CreateBoard(ctx context.Context, createBoard *pbBoard.CreateBoardDTO) (*pbBoard.NewBoardDTO, error)
	UpdateBoard(ctx context.Context, updateBoard *pbBoard.UpdateBoardDTO) (*pbBoard.NewBoardDTO, error)
	DeleteBoard(ctx context.Context, userID, boardID uint) (*pbBoard.Empty, error)
}
