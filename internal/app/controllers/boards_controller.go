package controllers

import (
	"context"
	"log"

	"github.com/mitchellh/mapstructure"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/converters"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/services"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/boards"
)

type BoardsController struct {
	BoardsService services.IBoardsService
}

func (bc BoardsController) GetBoards(
	ctx context.Context, filterBoards *pb.FilterBoards) ([]*pb.BoardsDTO, error) {
	var filterBoardsDTO filtering.BoardsFilter
	if err := mapstructure.Decode(filterBoards, &filterBoardsDTO); err != nil {
		log.Println("BoardsController.GetBoards error1:", err.Error())
		return nil, err
	}
	filterBoardsDTO.UserID = uint(filterBoards.UserId)
	filterBoardsDTO.MasterClassID = uint(filterBoards.MasterClassId)

	listBoards, err := bc.BoardsService.GetAllBoards(ctx, &filterBoardsDTO)
	if err != nil {
		log.Println("BoardsController.GetBoards error2:", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}
	l.Info("BoardsController.GetBoards: %v", listBoards)

	return converters.ConverterInPBListBoardsDTO(listBoards)
}

func (bc BoardsController) GetBoardByID(
	ctx context.Context, id uint) (*pb.BoardDTO, error) {
	board, err := bc.BoardsService.GetBoardByID(ctx, id)
	if err != nil {
		log.Println("BoardsController.GetBoardByID error:", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}
	l.Info("BoardsController.GetBoardByID: %v", board)

	return converters.ConverterInPBBoardDTO(board)
}

func (bc BoardsController) CreateBoard(
	ctx context.Context, createBoard *pb.CreateBoardDTO) (*pb.NewBoardDTO, error) {
	var board models.Boards
	if err := mapstructure.Decode(createBoard, &board); err != nil {
		log.Println("BoardsController.CreateBoard error:", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	newBoard, err := bc.BoardsService.CreateBoard(ctx, &board)
	if err != nil {
		log.Println("BoardsController.CreateBoard error:", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}
	l.Info("BoardsController.CreateBoard: %v:", newBoard)

	return converters.ConverterInPBNewBoardDTO(newBoard)
}

func (bc BoardsController) UpdateBoard(
	ctx context.Context, updateBoard *pb.UpdateBoardDTO) (*pb.NewBoardDTO, error) {
	var board models.Boards
	if err := mapstructure.Decode(updateBoard, &board); err != nil {
		log.Println("BoardsController.UpdateBoard error:", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	updBoard, err := bc.BoardsService.UpdateBoard(ctx, &board)
	if err != nil {
		log.Println("BoardsController.UpdateBoard error:", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}

	l.Info("BoardsController.UpdateBoard: %v", updBoard)
	return converters.ConverterInPBNewBoardDTO(updBoard)
}

func (bc BoardsController) DeleteBoard(
	ctx context.Context, userID, boardID uint) (*pb.Empty, error) {
	if err := bc.BoardsService.DeleteBoard(ctx, userID, boardID); err != nil {
		log.Println("BoardsController.DeleteBoard error:", err.Error())
		return nil, status.Errorf(codes.FailedPrecondition, err.Error())
	}
	l.Info("BoardsController.DeleteBoard: %v", boardID)
	return &pb.Empty{}, nil
}

// Подтвердждаем, что BoardsController реализует интерфейс IBoardsController
var _ IBoardsController = (*BoardsController)(nil)
