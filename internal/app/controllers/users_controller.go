package controllers

import (
	"context"
	"log"

	"github.com/mitchellh/mapstructure"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/converters"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/services"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
)

type UsersController struct {
	UsersService services.IUsersService
}

// Возвращает список всех пользователей
func (uc *UsersController) GetUsers(
	ctx context.Context, filterU *pb.FilterUsers) ([]*pb.UsersDTO, error) {

	var filterUsers filtering.UsersFilter
	if err := mapstructure.Decode(filterU, &filterUsers); err != nil {
		l.Error("UsersController.GetUsers error1: %v", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	listUsers, err := uc.UsersService.GetAllUsers(ctx, &filterUsers)
	if err != nil {
		l.Error("UsersController.GetUsers error2: %v", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}
	l.Info("UsersController.GetUsers listUsers: %v", listUsers)

	return converters.ConverterInPBListUsersDTO(listUsers)
}

// Возвращает пользователя по ID
// Param id path uint true "User ID"
// Success 200 {object} dto.UserDTO
// Router /user/{id} [get]
func (uc *UsersController) GetUserByID(
	ctx context.Context, id uint) (*pb.UserDTO, error) {
	user, err := uc.UsersService.GetUserByID(ctx, id)
	if err != nil {
		l.Error("UsersController.GetUserByID error: %v", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}
	l.Info("UsersController.GetUserByID: %v", user)

	return converters.ConverterInPBUserDTO(user)
}

// Проверяет данные пользователя по email и password
// Success 200 {object} dto.UserDTO
// Router /user/{id} [get]
func (uc *UsersController) LoginUser(
	ctx context.Context, email, password string) (*pb.UserDTO, error) {
	user, err := uc.UsersService.GetUserByEmail(ctx, email, password)
	if err != nil {
		l.Error("UsersController.LoginUser error: %v", err.Error())
		return nil, mistakes.HandleErrorUnauthenticatedConversion(err)
	}
	l.Info("UsersController.LoginUser: %v", user)

	return converters.ConverterInPBUserDTO(user)
}

// Создает нового пользователя
// Param user body models.Users true "User" // изменить входные
// Success 201 {object} models.User
// Router /register [post]
func (uc *UsersController) CreateUser(
	ctx context.Context, createUser *pb.CreateUserDTO) (*pb.NewUserDTO, error) {
	var user models.Users
	if err := mapstructure.Decode(createUser, &user); err != nil {
		l.Error("UsersController.CreateUser error1: %v", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	newUser, err := uc.UsersService.CreateUser(ctx, &user)
	if err != nil {
		l.Error("UsersController.CreateUser error2: %v", err.Error())
		return nil, mistakes.HandleErrorUnauthenticatedConversion(err)
	}
	l.Info("UsersController.CreateUser: %v", newUser)

	return converters.ConverterInPBNewUserDTO(newUser)

}

// Редактирует данные пользователя по ID
// Param id path uint true "User ID"
// Param user body models.Users true "User"
// Success 200 {object} models.User
// Router /user/{id} [patch]
func (uc *UsersController) UpdateUser(
	ctx context.Context, updateUser *pb.UpdateUserDTO) (*pb.NewUserDTO, error) {
	var user models.Users
	err := mapstructure.Decode(updateUser, &user)
	if err != nil {
		l.Error("UsersController.UpdateUser error1: %v", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	updUser, err := uc.UsersService.UpdateUser(ctx, &user)
	if err != nil {
		l.Error("UsersController.UpdateUser error2: %v", err.Error())
		return nil, mistakes.HandleErrorUnauthenticatedConversion(err)
	}
	l.Info("UsersController.UpdateUser: %v", &updUser)

	return converters.ConverterInPBNewUserDTO(updUser)
}

func (uc *UsersController) UpdatePassword(
	ctx context.Context, updatePassword *pb.UpdatePasswordDTO) (*pb.NewUserDTO, error) {
	var user models.Users
	err := mapstructure.Decode(updatePassword, &user)
	if err != nil {
		log.Println("UsersController.UpdatePassword error1:", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	updUser, err := uc.UsersService.UpdatePassword(ctx, &user)
	if err != nil {
		l.Error("UsersController.UpdatePassword error2: %v:", err.Error())
		return nil, mistakes.HandleErrorUnauthenticatedConversion(err)
	}
	l.Info("UsersController.UpdatePassword: %v", &updUser)

	return converters.ConverterInPBNewUserDTO(updUser)
}

// Удаляет пользователя по ID
// Param id path uint true "User ID"
// Success 204 "No content"
// Router /user/{id} [delete]
func (uc *UsersController) DeleteUser(
	ctx context.Context, id uint) (*pb.Empty, error) {
	if err := uc.UsersService.DeleteUser(ctx, id); err != nil {
		l.Error("UsersController.DeleteUser error: %v", err.Error())
		return nil, status.Errorf(codes.FailedPrecondition, err.Error())
	}

	l.Info("UsersController.DeleteUser")
	return &pb.Empty{}, nil
}

// Подтвердждаем, что UsersController реализует интерфейс IUsersController
var _ IUsersController = (*UsersController)(nil)
