package controllers

import (
	"context"
	"log"

	"github.com/mitchellh/mapstructure"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/converters"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/services"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/master_classes"
)

type MasterClassesController struct {
	MasterClassesService services.IMasterClassesService
}

// Возвращает список всех мастер-классов в системе
func (uc *MasterClassesController) GetMasterClasses(
	ctx context.Context, filterMC *pb.FilterMasterClasses) ([]*pb.MasterClassesDTO, error) {
	var filterMasterClasses filtering.MasterClassesFilter
	if err := mapstructure.Decode(filterMC, &filterMasterClasses); err != nil {
		log.Println("MasterClassController.GetMasterClasses error1:", err.Error())
		return nil, err
	}

	listMasterClasses, err := uc.MasterClassesService.GetAllMasterClasses(ctx, &filterMasterClasses)
	if err != nil {
		log.Println("MasterClassController.GetMasterClasses error2:", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}
	l.Info("MasterClassController.GetMasterClasses: %v", listMasterClasses)

	return converters.ConverterInPBListMasterClassesDTO(listMasterClasses)
}

// Возвращает мастер-класс по ID
func (uc *MasterClassesController) GetMasterClassByID(
	ctx context.Context, id uint) (*pb.MasterClassDTO, error) {
	masterClass, err := uc.MasterClassesService.GetMasterClassByID(ctx, id)
	if err != nil {
		log.Println("MasterClassController.GetMasterClassByID error1:", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}
	l.Info("MasterClassController.GetMasterClassByID: %v", masterClass)

	return converters.ConverterInPBMasterClassDTO(masterClass)
}

// Создает новый мастер-класса
func (uc *MasterClassesController) CreateMasterClass(
	ctx context.Context, createMC *pb.CreateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	var masterClass models.MasterClasses
	if err := mapstructure.Decode(createMC, &masterClass); err != nil {
		log.Println("MasterClassController.CreateMasterClass error1:", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	newMC, err := uc.MasterClassesService.CreateMasterClass(ctx, &masterClass)
	if err != nil {
		log.Println("MasterClassController.CreateMasterClass error2:", err.Error())
		return nil, mistakes.HandleErrorConversion(err)
	}
	l.Info("MasterClassController.CreateMasterClass: %v", newMC)

	return converters.ConverterInPBNewMasterClassDTO(newMC)
}

// Изменят роль пользователя и создает мастер-класс
func (uc *MasterClassesController) UpdateUserCreateMC(
	ctx context.Context, updateMC *pb.CreateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	var masterClass models.MasterClasses
	if err := mapstructure.Decode(updateMC, &masterClass); err != nil {
		log.Println("MasterClassController.UpdateUserCreateMC error1:", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	updMC, err := uc.MasterClassesService.UpdateUserCreateMC(ctx, &masterClass)
	if err != nil {
		log.Println("MasterClassController.UpdateUserCreateMC error2:", err.Error())
		return nil, mistakes.HandleErrorNotFoundConversion(err)
	}
	l.Info("MasterClassController.UpdateUserCreateMC: %v", updMC)

	return converters.ConverterInPBNewMasterClassDTO(updMC)
}

// Изменяет данные мастер-класса по ID
func (uc *MasterClassesController) UpdateMasterClass(
	ctx context.Context, updateMC *pb.UpdateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	var masterClass models.MasterClasses
	if err := mapstructure.Decode(updateMC, &masterClass); err != nil {
		log.Println("MasterClassController.UpdateMasterClass error1:", err.Error())
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	updMasterClass, err := uc.MasterClassesService.UpdateMasterClass(ctx, &masterClass)
	if err != nil {
		log.Println("MasterClassController.UpdateMasterClass error2:", err.Error())
		return nil, mistakes.HandleErrorConversion(err)
	}
	l.Info("MasterClassController.UpdateMasterClass: %v", updMasterClass)

	return converters.ConverterInPBNewMasterClassDTO(updMasterClass)
}

// Удаляет мастер-класс по ID
func (uc *MasterClassesController) DeleteMasterClass(
	ctx context.Context, authorID, masterClassID uint) (*pb.Empty, error) {
	if err := uc.MasterClassesService.DeleteMasterClass(ctx, authorID, masterClassID); err != nil {
		log.Println("MasterClassController.DeleteMasterClass error1:", err.Error())
		return nil, status.Errorf(codes.FailedPrecondition, err.Error())
	}

	l.Info("MasterClassController.DeleteMasterClass: %v", masterClassID)

	return &pb.Empty{}, nil
}

// Подтвердждаем, что MasterClassesController реализует интерфейс IMasterClassesController
var _ IMasterClassesController = (*MasterClassesController)(nil)
