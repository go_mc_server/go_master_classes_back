package grpc_server

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/controllers"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
)

//type GrpcUsersInterface interface {
//	LoginUser(context.Context, *UserLoginDetails) (*UserDTO, error)
//	GetUsers(*FilterUsers, Users_GetUsersServer) error
//	GetUserByID(context.Context, *UserID) (*UserDTO, error)
//	CreateUser(context.Context, *CreateUpdateUser) (*NewUserDTO, error)
//	UpdateUser(context.Context, *CreateUpdateUser) (*NewUserDTO, error)
//	DeleteUser(context.Context, *UserID) (*Empty, error)
//}

type GrpcUsersServer struct {
	pb.UnimplementedUsersServer
	controller controllers.IUsersController
}

// Аутентификация пользователя
func (s GrpcUsersServer) LoginUser(ctx context.Context, req *pb.UserLoginDetails) (*pb.UserDTO, error) {
	l.Important("Received LoginUser %v", req.Email)

	userDTO, err := s.controller.LoginUser(ctx, req.Email, req.Password)
	if err != nil {
		l.Error("Error LoginUser: %v", err)
		return nil, err
	}
	if userDTO.Id == 0 {
		l.Warn("LoginUser: %v", userDTO.Id)
		return nil, status.Error(codes.NotFound, "User not found")
	}
	l.Important("Success LoginUser: %v", userDTO)
	return userDTO, nil
}

// получить список пользователей с фильтрами
func (s GrpcUsersServer) GetUsers(ctx context.Context, req *pb.FilterUsers) (*pb.ListUsers, error) {
	l.Important("Received GetUsers %v", req)

	usersDTO, err := s.controller.GetUsers(ctx, req)
	if err != nil {
		l.Error("Error GetUsers: %v", err)
		return nil, err
	}
	l.Important("Success GetUsers: %v", usersDTO)
	return &pb.ListUsers{ListUsers: usersDTO}, nil
}

// получить пользователя по id
func (s GrpcUsersServer) GetUserByID(ctx context.Context, req *pb.UserID) (*pb.UserDTO, error) {
	l.Important("Received GetUserByID %v", req)
	userDTO, err := s.controller.GetUserByID(ctx, uint(req.Id))
	if err != nil {
		l.Error("Error GetUserByID: %v", err)
		return nil, err
	}

	l.Important("Success GetUserByID: %v", userDTO)
	return userDTO, nil
}

func (s GrpcUsersServer) CreateUser(ctx context.Context, req *pb.CreateUserDTO) (*pb.NewUserDTO, error) {
	l.Important("Received CreateUser %v", req)
	newUserDTO, err := s.controller.CreateUser(ctx, req)
	if err != nil {
		l.Error("Error CreateUser: %v", err)
		return nil, err
	}

	l.Important("Success CreateUser: %v", newUserDTO)
	return newUserDTO, nil
}

func (s GrpcUsersServer) UpdateUser(ctx context.Context, req *pb.UpdateUserDTO) (*pb.NewUserDTO, error) {
	l.Important("Received UpdateUser %v", req)
	updateUserDTO, err := s.controller.UpdateUser(ctx, req)
	if err != nil {
		l.Error("Error UpdateUser: %v", err)
		return nil, err
	}
	l.Important("Success UpdateUser: %v", updateUserDTO)
	return updateUserDTO, nil
}

func (s GrpcUsersServer) UpdatePasswordUser(ctx context.Context, req *pb.UpdatePasswordDTO) (*pb.NewUserDTO, error) {
	l.Important("Received UpdatePasswordUser %v", req)
	updatePasswordDTO, err := s.controller.UpdatePassword(ctx, req)
	if err != nil {
		l.Error("Error UpdatePasswordUser: %v", err)
		return nil, err
	}
	l.Important("Success UpdatePasswordUser: %v", updatePasswordDTO)
	return updatePasswordDTO, nil
}

func (s GrpcUsersServer) DeleteUser(ctx context.Context, req *pb.UserID) (*pb.Empty, error) {
	l.Important("Received DeleteUser %v", req)
	deleteUser, err := s.controller.DeleteUser(ctx, uint(req.Id))
	if err != nil {
		l.Error("Error DeleteUser: %v", err)
		return nil, err
	}
	l.Important("Success DeleteUser: %v", deleteUser)
	return deleteUser, nil
}
