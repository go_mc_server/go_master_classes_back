package grpc_server

import (
	"context"
	"log"

	"gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/tex"
)

type TexServer struct {
	tex.UnimplementedTexServer
}

func (s *TexServer) GetTex(ctx context.Context, req *tex.TexRequest) (*tex.TexResponse, error) {
	log.Printf("Received GetText: %v", req.GetText())
	return &tex.TexResponse{Text: "Тут твой текст: " + req.GetText()}, nil
}
