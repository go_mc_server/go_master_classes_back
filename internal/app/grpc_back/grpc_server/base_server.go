package grpc_server

import (
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"google.golang.org/grpc"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/config"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/controllers"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/repositories"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/services"
	pbBoards "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/boards"
	pbMC "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/master_classes"
	pbTex "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/tex"
	pbUsers "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
)

func RunGRPCServer() {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pbTex.RegisterTexServer(s, &TexServer{})
	pbUsers.RegisterUsersServer(s, users())
	pbMC.RegisterMasterClassesServer(s, masterClasses())
	pbBoards.RegisterBoardsServer(s, boards())

	// Канал для захвата сигналов от операционной системы.
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	// Запуск сервера в отдельной горутине
	go runGRPCServer(s, lis)

	// Ожидание сигнала завершения
	<-sigs

	l.ApiInfo("Graceful shutdown initiated")
	s.GracefulStop() // Плавное завершение gRPC-сервера.
	l.ApiInfo("gRPC server stopped")
}

func runGRPCServer(s *grpc.Server, lis net.Listener) {
	l.ApiInfo("gRPC server started on :50051")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func users() *GrpcUsersServer {
	usersRepository := &repositories.UsersRepository{DB: config.DB}
	usersService := &services.UsersService{UsersRepo: usersRepository}
	usersController := &controllers.UsersController{UsersService: usersService}
	return &GrpcUsersServer{controller: usersController}
}

func masterClasses() *GrpcMasterClassesServer {
	masterClassesRepository := &repositories.MasterClassesRepository{DB: config.DB}
	masterClassesService := &services.MasterClassesService{MasterClassRepo: masterClassesRepository}
	masterClassesController := &controllers.MasterClassesController{MasterClassesService: masterClassesService}
	return &GrpcMasterClassesServer{controller: masterClassesController}
}

func boards() *GrpcBoardsServer {
	boardsRepository := &repositories.BoardsRepository{DB: config.DB}
	boardsService := &services.BoardsService{BoardsRepo: boardsRepository}
	boardsController := &controllers.BoardsController{BoardsService: boardsService}
	return &GrpcBoardsServer{controller: boardsController}
}
