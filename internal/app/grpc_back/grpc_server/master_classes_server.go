package grpc_server

import (
	"context"
	"log"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/controllers"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/master_classes"
)

type GrpcMasterClassesServer struct {
	pb.UnimplementedMasterClassesServer
	controller controllers.IMasterClassesController
}

func (s GrpcMasterClassesServer) GetMasterClasses(
	ctx context.Context, req *pb.FilterMasterClasses) (*pb.ListMasterClasses, error) {
	l.Important("Received GetMasterClasses %v", req)

	masterClassesDTO, err := s.controller.GetMasterClasses(ctx, req)
	if err != nil {
		log.Printf("Error GetMasterClasses: %v", err)
		return nil, err
	}

	l.Important("Success GetMasterClasses: %v", masterClassesDTO)
	return &pb.ListMasterClasses{ListMasterClasses: masterClassesDTO}, nil
}

func (s GrpcMasterClassesServer) GetMasterClassByID(
	ctx context.Context, req *pb.MasterClassID) (*pb.MasterClassDTO, error) {
	l.Important("Received GetMasterClassByID %v", req)
	masterClassDTO, err := s.controller.GetMasterClassByID(ctx, uint(req.Id))
	if err != nil {
		log.Printf("Error GetMasterClassByID: %v", err)
		return nil, err
	}
	l.Important("Success GetMasterClassByID: %v", masterClassDTO)
	return masterClassDTO, nil
}

func (s GrpcMasterClassesServer) CreateMasterClass(
	ctx context.Context, req *pb.CreateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	l.Important("Received CreateMasterClass %v", req)
	newMasterClass, err := s.controller.CreateMasterClass(ctx, req)
	if err != nil {
		log.Printf("Error CreateMasterClass: %v", err)
		return nil, err
	}
	l.Important("Success CreateMasterClass: %v", newMasterClass)
	return newMasterClass, nil
}

func (s GrpcMasterClassesServer) UpdateUserCreateMC(
	ctx context.Context, req *pb.CreateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	l.Important("Received UpdateUserCreateMC %v", req)
	newMasterClass, err := s.controller.UpdateUserCreateMC(ctx, req)
	if err != nil {
		log.Printf("Error UpdateUserCreateMC: %v", err)
		return nil, err
	}
	l.Important("Success UpdateUserCreateMC: %v", newMasterClass)
	return newMasterClass, nil
}

func (s GrpcMasterClassesServer) UpdateMasterClass(
	ctx context.Context, req *pb.UpdateMasterClassDTO) (*pb.NewMasterClassDTO, error) {
	l.Important("Received UpdateMasterClass %v", req)
	newMasterClass, err := s.controller.UpdateMasterClass(ctx, req)
	if err != nil {
		log.Printf("Error UpdateMasterClass: %v", err)
		return nil, err
	}
	l.Important("Success UpdateMasterClass: %v", newMasterClass)
	return newMasterClass, nil
}

func (s GrpcMasterClassesServer) DeleteMasterClass(
	ctx context.Context, req *pb.UserMasterClassID) (*pb.Empty, error) {
	l.Important("Received DeleteMasterClass %v", req)
	deleteMasterClass, err := s.controller.DeleteMasterClass(ctx, uint(req.IdAuthor), uint(req.IdMc))
	if err != nil {
		log.Printf("Error DeleteMasterClass: %v", err)
		return nil, err
	}
	l.Important("Success DeleteMasterClass: %v", deleteMasterClass)
	return deleteMasterClass, nil
}
