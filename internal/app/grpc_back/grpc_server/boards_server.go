package grpc_server

import (
	"context"
	"log"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/controllers"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/boards"
)

type GrpcBoardsServer struct {
	pb.UnimplementedBoardsServer
	controller controllers.IBoardsController
}

func (s GrpcBoardsServer) GetBoards(
	ctx context.Context, req *pb.FilterBoards) (*pb.ListBoards, error) {
	l.Important("Received GetBoards %v", req)

	boardsDTO, err := s.controller.GetBoards(ctx, req)
	if err != nil {
		log.Printf("Error GetBoards: %v", err)
		return nil, err
	}

	l.Important("Success GetBoards: %v", boardsDTO)
	return &pb.ListBoards{ListBoards: boardsDTO}, nil
}

func (s GrpcBoardsServer) GetBoardByID(
	ctx context.Context, req *pb.BoardID) (*pb.BoardDTO, error) {
	l.Important("Received GetBoardByID %v", req)

	boardDTO, err := s.controller.GetBoardByID(ctx, uint(req.Id))
	if err != nil {
		log.Printf("Error GetBoardByID: %v", err)
		return nil, err
	}

	l.Important("Success GetBoardByID: %v", boardDTO)
	return boardDTO, nil
}

func (s GrpcBoardsServer) CreateBoard(
	ctx context.Context, req *pb.CreateBoardDTO) (*pb.NewBoardDTO, error) {
	l.Important("Received CreateBoard %v", req)

	boardDTO, err := s.controller.CreateBoard(ctx, req)
	if err != nil {
		log.Printf("Error CreateBoard: %v", err)
		return nil, err
	}

	l.Important("Success CreateBoard: %v", boardDTO)
	return boardDTO, nil
}

func (s GrpcBoardsServer) UpdateBoard(
	ctx context.Context, req *pb.UpdateBoardDTO) (*pb.NewBoardDTO, error) {
	l.Important("Received UpdateBoard %v", req)

	boardDTO, err := s.controller.UpdateBoard(ctx, req)
	if err != nil {
		log.Printf("Error UpdateBoard: %v", err)
		return nil, err
	}

	l.Important("Success UpdateBoard: %v", boardDTO)
	return boardDTO, nil
}

func (s GrpcBoardsServer) DeleteBoard(
	ctx context.Context, req *pb.UserBoardID) (*pb.Empty, error) {
	l.Important("Received DeleteBoard %v", req)
	deleteBoard, err := s.controller.DeleteBoard(ctx, uint(req.UserId), uint(req.BoardId))
	if err != nil {
		log.Printf("Error DeleteBoard: %v", err)
		return nil, err
	}
	l.Important("Success DeleteBoard: %v", deleteBoard)
	return deleteBoard, nil
}
