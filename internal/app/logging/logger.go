package logging

import (
	"log"
	"os"
)

// SetupLogger настраивает логгер для вывода в stdout
func SetupLogger() {
	// Настраиваем логгер для вывода в stdout
	log.SetOutput(os.Stdout)

	// Устанавливаем префикс лог-сообщений
	log.SetPrefix("LOG: ")

	// Устанавливаем флаги логгера (включить дату и время)
	log.SetFlags(log.Ldate | log.Ltime)
}

// isLocalEnv функция, определяющая, является ли окружение локальным
func isLocalEnv() bool {
	env := os.Getenv("ENV")
	return env == "local"
}
