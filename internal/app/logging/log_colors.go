package logging

import "log"

// информационные сообщения - зеленая
func Info(format string, v ...interface{}) {
	if isLocalEnv() {
		log.Printf(Green+format+Reset, v...)
	} else {
		log.Printf(format, v...)
	}
}

// предупреждение - желтый
func Warn(format string, v ...interface{}) {
	if isLocalEnv() {
		log.Printf(Yellow+format+Reset, v...)
	} else {
		log.Printf(format, v...)
	}
}

// ошибки - красный
func Error(format string, v ...interface{}) {
	if isLocalEnv() {
		log.Printf(Red+format+Reset, v...)
	} else {
		log.Printf(format, v...)
	}

}

// отладочные сообщения - синий
func Debug(format string, v ...interface{}) {
	if isLocalEnv() {
		log.Printf(Blue+format+Reset, v...)
	} else {
		log.Printf(format, v...)
	}
}

// начало/конец блоков, идентификация переменных  - магента
func Important(format string, v ...interface{}) {
	if isLocalEnv() {
		log.Printf(Magenta+format+Reset, v...)
	} else {
		log.Printf(format, v...)
	}
}

// состояние подключения - циан
func ApiInfo(format string, v ...interface{}) {
	if isLocalEnv() {
		log.Printf(Cyan+format+Reset, v...)
	} else {
		log.Printf(format, v...)
	}
}

// общая информация - белый
func GeneralInfo(format string, v ...interface{}) {
	if isLocalEnv() {
		log.Printf(White+format+Reset, v...)
	} else {
		log.Printf(format, v...)
	}
}
