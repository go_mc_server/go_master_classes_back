package config

import (
	"fmt"
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/migrations"
)

var (
	DB *gorm.DB
)

func InitDB() {
	//Получение значений переменных окружения
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbName := os.Getenv("DB_NAME")
	//режим использования SSL.
	dbSSLMode := os.Getenv("DB_SSLMODE")

	// Проверка, что все переменные окружения установлены
	//if dbUser == "" || dbPassword == "" || dbHost == "" || dbPort == "" || dbName == "" || dbSSLMode == "" {
	//	log.Fatal("Отсутствует одна или несколько обязательных переменных среды окружения.")
	//}
	switch {
	case dbUser == "":
		log.Fatalln("Отсутствует обязательная переменная в .env: DB_USER")
	case dbPassword == "":
		log.Fatalln("Отсутствует обязательная переменная в .env: DB_PASSWORD")
	case dbHost == "":
		log.Fatalln("Отсутствует обязательная переменная в .env: DB_HOST")
	case dbPort == "":
		log.Fatalln("Отсутствует обязательная переменная в .env: DB_PORT")
	case dbName == "":
		log.Fatalln("Отсутствует обязательная переменная в .env: DB_NAME")
	case dbSSLMode == "":
		log.Fatalln("Отсутствует обязательная переменная в .env: DB_SSLMODE")
	default:
		l.Important("Все переменные окружения установлены")
	}

	// Подключение к бд
	// DATABASE_URL=postgresql://user:password@localhost:5432/database_name?sslmode=disable
	dsn := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s?sslmode=%s",
		dbUser, dbPassword, dbHost, dbPort, dbName, dbSSLMode)

	var err error
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("Не удалось подключиться к базе данных." + err.Error())
	}
	l.Important("Подключение к базе данныйх успешно установлено")

	// Применение миграций
	migrations.RunMigrations(DB)
}
