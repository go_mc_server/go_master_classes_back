package converters

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/dto"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/users"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// конвертация в внутренние dto
func ConverterInListUsersDTO(
	users *[]models.Users) ([]*dto.UserDTO, error) {
	var listUsers []*dto.UserDTO
	for _, user := range *users {
		var userDTO *dto.UserDTO
		if err := mapstructure.Decode(user, &userDTO); err != nil {
			l.Error("ConverterInListUsersDTO error: %v", err.Error())
			return nil, mistakes.ErrConversion
		}
		userDTO.CreatedAt = user.CreatedAt
		userDTO.UpdatedAt = user.UpdatedAt
		listUsers = append(listUsers, userDTO)
	}

	return listUsers, nil
}

func ConverterInUserDTO(
	user *models.Users) (*dto.UserDTO, error) {
	var userDTO *dto.UserDTO
	if err := mapstructure.Decode(user, &userDTO); err != nil {
		l.Error("ConverterInUserDTO error: %v", err.Error())
		return nil, mistakes.ErrConversion
	}
	userDTO.CreatedAt = user.CreatedAt
	userDTO.UpdatedAt = user.UpdatedAt

	return userDTO, nil
}

func ConverterInNewUserDTO(
	user *models.Users) (*dto.UserDetailDTO, error) {
	var newUserDTO *dto.UserDetailDTO
	if err := mapstructure.Decode(user, &newUserDTO); err != nil {
		l.Error("ConverterInNewUserDTO error: %v", err.Error())
		return nil, mistakes.ErrConversion
	}
	newUserDTO.CreatedAt = user.CreatedAt
	newUserDTO.UpdatedAt = user.UpdatedAt

	return newUserDTO, nil
}

// конвертации в модели grps proto server
func ConverterInPBListUsersDTO(
	users []*dto.UserDTO) ([]*pb.UsersDTO, error) {
	var listUsers []*pb.UsersDTO
	for _, user := range users {
		var userDTO *pb.UsersDTO
		if err := mapstructure.Decode(user, &userDTO); err != nil {
			l.Error("ConverterInListUsersDTO error: %v", err.Error())
			return nil, mistakes.ErrConversion
		}
		listUsers = append(listUsers, userDTO)
	}

	return listUsers, nil
}

func ConverterInPBUserDTO(
	user *dto.UserDTO) (*pb.UserDTO, error) {
	var userDTO *pb.UserDTO
	if err := mapstructure.Decode(user, &userDTO); err != nil {
		l.Error("ConverterInUserDTO error: %v", err.Error())
		return nil, mistakes.ErrConversion
	}
	userDTO.CreatedAt = timestamppb.New(user.CreatedAt)
	userDTO.UpdatedAt = timestamppb.New(user.UpdatedAt)

	return userDTO, nil
}

func ConverterInPBNewUserDTO(
	user *dto.UserDetailDTO) (*pb.NewUserDTO, error) {
	var newUserDTO *pb.NewUserDTO
	if err := mapstructure.Decode(user, &newUserDTO); err != nil {
		l.Error("ConverterInNewUserDTO error: %v", err.Error())
		return nil, mistakes.ErrConversion
	}
	newUserDTO.CreatedAt = timestamppb.New(user.CreatedAt)
	newUserDTO.UpdatedAt = timestamppb.New(user.UpdatedAt)

	return newUserDTO, nil
}
