package converters

import (
	"log"

	"github.com/mitchellh/mapstructure"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/dto"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/boards"
)

// конвертация в внутренние dto
func ConverterInListBoardsDTO(
	boards *[]models.Boards) ([]*dto.BoardDTO, error) {
	var listBoards []*dto.BoardDTO
	for _, board := range *boards {
		var boardDTO *dto.BoardDTO
		if err := mapstructure.Decode(board, &boardDTO); err != nil {
			log.Println("ConverterInListBoardsDTO error:", err.Error())
			return nil, mistakes.ErrConversion
		}
		boardDTO.CreatedAt = board.CreatedAt
		boardDTO.UpdatedAt = board.UpdatedAt
		listBoards = append(listBoards, boardDTO)
	}
	return listBoards, nil
}

func ConverterInBoardDetailDTO(
	board *models.Boards) (*dto.BoardDetailDTO, error) {
	var boardDTO *dto.BoardDetailDTO
	if err := mapstructure.Decode(board, &boardDTO); err != nil {
		log.Println("ConverterInBoardDetailDTO error:", err.Error())
		return nil, mistakes.ErrConversion
	}
	boardDTO.CreatedAt = board.CreatedAt
	boardDTO.UpdatedAt = board.UpdatedAt
	return boardDTO, nil
}

func ConverterInNewBoardDTO(
	board *models.Boards) (*dto.BoardDTO, error) {
	var boardDTO *dto.BoardDTO
	err := mapstructure.Decode(board, &boardDTO)
	if err != nil {
		log.Println("ConverterInNewBoardDTO error:", err.Error())
		return nil, mistakes.ErrConversion
	}
	boardDTO.CreatedAt = board.CreatedAt
	boardDTO.UpdatedAt = board.UpdatedAt
	return boardDTO, nil
}

// конвертации в модели grps proto server
func ConverterInPBListBoardsDTO(
	boards []*dto.BoardDTO) ([]*pb.BoardsDTO, error) {
	var listBoards []*pb.BoardsDTO
	for _, board := range boards {
		var boardDTO *pb.BoardsDTO
		if err := mapstructure.Decode(board, &boardDTO); err != nil {
			log.Println("ConverterInPBListBoardsDTO error:", err.Error())
			return nil, mistakes.ErrConversion
		}
		listBoards = append(listBoards, boardDTO)
	}
	return listBoards, nil
}

func ConverterInPBBoardDTO(
	board *dto.BoardDetailDTO) (*pb.BoardDTO, error) {
	var boardDTO *pb.BoardDTO
	if err := mapstructure.Decode(board, &boardDTO); err != nil {
		log.Println("ConverterInPBBoardDTO error:", err.Error())
		return nil, mistakes.ErrConversion
	}
	boardDTO.CreatedAt = timestamppb.New(board.CreatedAt)
	boardDTO.UpdatedAt = timestamppb.New(board.UpdatedAt)
	return boardDTO, nil
}

func ConverterInPBNewBoardDTO(
	board *dto.BoardDTO) (*pb.NewBoardDTO, error) {
	var boardDTO *pb.NewBoardDTO
	err := mapstructure.Decode(board, &boardDTO)
	if err != nil {
		log.Println("ConverterInPBNewBoardDTO error:", err.Error())
		return nil, mistakes.ErrConversion
	}
	boardDTO.CreatedAt = timestamppb.New(board.CreatedAt)
	boardDTO.UpdatedAt = timestamppb.New(board.UpdatedAt)
	return boardDTO, nil
}
