package converters

import (
	"log"

	"github.com/mitchellh/mapstructure"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/dto"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	pb "gitlab.com/go_mc_server/go_master_classes_back/pkg/proto/master_classes"
)

// конвертация в внутренние dto
func ConverterInListMasterClassesDTO(
	masterClasses *[]models.MasterClasses) ([]*dto.MasterClassDTO, error) {
	var listMasterClasses []*dto.MasterClassDTO
	for _, masterClass := range *masterClasses {
		var mcDTO *dto.MasterClassDTO
		if err := mapstructure.Decode(masterClass, &mcDTO); err != nil {
			log.Println("ConverterInListMasterClassesDTO error:", err.Error())
			return nil, mistakes.ErrConversion
		}
		mcDTO.CreatedAt = masterClass.CreatedAt
		mcDTO.UpdatedAt = masterClass.UpdatedAt
		listMasterClasses = append(listMasterClasses, mcDTO)
	}

	return listMasterClasses, nil
}

func ConverterInMasterClassDTO(
	masterClass *models.MasterClasses) (*dto.MasterClassDetailDTO, error) {
	/*
		authorDTO := dto.UserInfoDTO{
			ID:         masterClass.Author.ID,
			LastName:   masterClass.Author.LastName,
			FirstName:  masterClass.Author.FirstName,
			MiddleName: masterClass.Author.MiddleName,
			Email:      masterClass.Author.Email,
		}

		masterClassDetailDTO := dto.MasterClassDetailDTO{
			Author:         authorDTO,
			TypeNeedlework: masterClass.TypeNeedlework,
			Name:           masterClass.Name,
			Description:    masterClass.Description,
			Materials:      masterClass.Materials,
			IsPaid:         masterClass.IsPaid,
			Price:          masterClass.Price,
		}*/
	var masterClassDetailDTO *dto.MasterClassDetailDTO
	if err := mapstructure.Decode(masterClass, &masterClassDetailDTO); err != nil {
		log.Println("ConverterInMasterClassDTO error:", err.Error())
		return nil, mistakes.ErrConversion
	}
	masterClassDetailDTO.CreatedAt = masterClass.CreatedAt
	masterClassDetailDTO.UpdatedAt = masterClass.UpdatedAt

	return masterClassDetailDTO, nil
}

func ConverterInNewMasterClassDTO(
	masterClass *models.MasterClasses) (*dto.MasterClassDTO, error) {
	var newMasterClassDTO *dto.MasterClassDTO
	err := mapstructure.Decode(masterClass, &newMasterClassDTO)
	if err != nil {
		log.Println("ConverterInNewMasterClassDTO error:", err.Error())
		return nil, mistakes.ErrConversion
	}
	newMasterClassDTO.CreatedAt = masterClass.CreatedAt
	newMasterClassDTO.UpdatedAt = masterClass.UpdatedAt

	return newMasterClassDTO, nil
}

// конвертации в модели grps proto server
func ConverterInPBListMasterClassesDTO(
	masterClasses []*dto.MasterClassDTO) ([]*pb.MasterClassesDTO, error) {
	var listMasterClasses []*pb.MasterClassesDTO
	for _, masterClass := range masterClasses {
		var mcDTO *pb.MasterClassesDTO
		if err := mapstructure.Decode(masterClass, &mcDTO); err != nil {
			log.Println("ConverterInPBListMasterClassesDTO error:", err.Error())
			return nil, mistakes.ErrConversion
		}
		listMasterClasses = append(listMasterClasses, mcDTO)
	}

	return listMasterClasses, nil
}

func ConverterInPBMasterClassDTO(
	masterClass *dto.MasterClassDetailDTO) (*pb.MasterClassDTO, error) {
	var masterClassDetailDTO *pb.MasterClassDTO
	if err := mapstructure.Decode(masterClass, &masterClassDetailDTO); err != nil {
		log.Println("ConverterInPBMasterClassDTO error:", err.Error())
		return nil, mistakes.ErrConversion
	}
	masterClassDetailDTO.CreatedAt = timestamppb.New(masterClass.CreatedAt)
	masterClassDetailDTO.UpdatedAt = timestamppb.New(masterClass.UpdatedAt)

	return masterClassDetailDTO, nil
}

func ConverterInPBNewMasterClassDTO(
	masterClass *dto.MasterClassDTO) (*pb.NewMasterClassDTO, error) {
	var newMasterClassDTO *pb.NewMasterClassDTO
	err := mapstructure.Decode(masterClass, &newMasterClassDTO)
	if err != nil {
		log.Println("ConverterInPBNewMasterClassDTO error:", err.Error())
		return nil, mistakes.ErrConversion
	}
	newMasterClassDTO.CreatedAt = timestamppb.New(masterClass.CreatedAt)
	newMasterClassDTO.UpdatedAt = timestamppb.New(masterClass.UpdatedAt)

	return newMasterClassDTO, nil
}
