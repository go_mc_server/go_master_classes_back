package mistakes

import (
	"errors"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func HandleErrorNotFoundConversion(err error) error {
	if errors.Is(err, ErrNotFound) {
		return status.Errorf(codes.OK, "not found")
	} else if errors.Is(err, ErrConversion) {
		return status.Errorf(codes.InvalidArgument, err.Error())
	} else {
		return status.Errorf(codes.FailedPrecondition, err.Error())
	}
}

func HandleErrorConversion(err error) error {
	if errors.Is(err, ErrConversion) {
		return status.Errorf(codes.InvalidArgument, err.Error())
	} else {
		return status.Errorf(codes.FailedPrecondition, err.Error())
	}
}

func HandleErrorUnauthenticatedConversion(err error) error {
	if errors.Is(err, ErrUnauthenticated) {
		return status.Errorf(codes.Unauthenticated, "unauthorized")
	} else if errors.Is(err, ErrConversion) {
		return status.Errorf(codes.InvalidArgument, err.Error())
	} else {
		return status.Errorf(codes.FailedPrecondition, err.Error())
	}
}
