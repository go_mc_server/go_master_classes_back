package mistakes

import "errors"

var (
	ErrNotFound        = errors.New("not found")
	ErrConversion      = errors.New("conversion error")
	ErrUnauthenticated = errors.New("unauthenticated")
)
