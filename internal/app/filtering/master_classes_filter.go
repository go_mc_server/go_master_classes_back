package filtering

import "gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"

type MasterClassesFilter struct {
	AuthorID       uint                  `json:"author_id,omitempty"`
	TypeNeedlework models.TypeNeedlework `json:"type_needlework,omitempty"`
	Name           string                `json:"name,omitempty"`
	Materials      string                `json:"materials,omitempty"`
	IsActive       *bool                 `json:"is_active,omitempty"`
}
