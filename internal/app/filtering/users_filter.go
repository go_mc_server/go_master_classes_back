package filtering

import "gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"

type UsersFilter struct {
	LastName   string          `json:"last_name,omitempty"`
	FirstName  string          `json:"first_name,omitempty"`
	MiddleName string          `json:"middle_name,omitempty"`
	Email      string          `json:"email,omitempty"`
	Role       models.RoleType `json:"role,omitempty"`
}
