package filtering

import "gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"

type BoardsFilter struct {
	UserID        uint                 `json:"user_id,omitempty"`
	MasterClassID uint                 `json:"master_class_id,omitempty"`
	CurrentStatus models.CurrentStatus `json:"current_status,omitempty"`
}
