// Бизнес-логика приложения,
// которая обычно комбинирует и оркестрирует данные, вызывая методы из репозиториев.
package services

import (
	"context"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/auth"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/converters"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/dto"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/repositories"
)

type UsersService struct {
	UsersRepo repositories.IUsersRepository
}

func (s *UsersService) GetAllUsers(
	ctx context.Context, filterUser *filtering.UsersFilter) ([]*dto.UserDTO, error) {
	users, err := s.UsersRepo.FindAll(ctx, filterUser)
	if err != nil {
		return nil, err
	}

	if len(*users) == 0 {
		l.Warn("UsersService.GetAllUsers no users found")
		return nil, mistakes.ErrNotFound
	}

	return converters.ConverterInListUsersDTO(users)
}

func (s *UsersService) GetUserByID(
	ctx context.Context, id uint) (*dto.UserDTO, error) {
	user, err := s.UsersRepo.FindByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInUserDTO(user)
}

func (s *UsersService) GetUserByEmail(
	ctx context.Context, email, password string) (*dto.UserDTO, error) {
	user, err := s.UsersRepo.FindByEmail(ctx, email)
	if err != nil {
		return nil, err
	}

	if auth.CheckPasswordHash(password, user.Password) {
		return converters.ConverterInUserDTO(user)
	} else {
		l.Warn("not auth.CheckPasswordHash")
		return nil, mistakes.ErrUnauthenticated
	}
}

func (s *UsersService) CreateUser(
	ctx context.Context, user *models.Users) (*dto.UserDetailDTO, error) {
	//хэшируем пароль пользователя
	hashedPassword, err := auth.HashPassword(user.Password)
	if err != nil {
		return nil, err
	}
	user.Password = hashedPassword

	if err = s.UsersRepo.Create(ctx, user); err != nil {
		return nil, err
	}

	return converters.ConverterInNewUserDTO(user)
}

func (s *UsersService) UpdateUser(
	ctx context.Context, user *models.Users) (*dto.UserDetailDTO, error) {
	if user.Password != "" {
		hashedPassword, err := auth.HashPassword(user.Password)
		if err != nil {
			return nil, err
		}
		user.Password = hashedPassword
	}

	updateUser, err := s.UsersRepo.Update(ctx, user)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInNewUserDTO(updateUser)
}

func (s *UsersService) UpdatePassword(
	ctx context.Context, user *models.Users) (*dto.UserDetailDTO, error) {
	hashedPassword, err := auth.HashPassword(user.Password)
	if err != nil {
		return nil, err
	}
	user.Password = hashedPassword

	updateUser, err := s.UsersRepo.UpdatePassword(ctx, user)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInNewUserDTO(updateUser)

}

func (s *UsersService) DeleteUser(
	ctx context.Context, id uint) error {
	return s.UsersRepo.Delete(ctx, id)
}

// Подтвердждаем, что UsersService реализует интерфейс IUsersService
var _ IUsersService = (*UsersService)(nil)
