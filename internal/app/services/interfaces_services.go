package services

import (
	"context"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/dto"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
)

type IUsersService interface {
	GetAllUsers(ctx context.Context, filterUser *filtering.UsersFilter) ([]*dto.UserDTO, error)
	GetUserByID(ctx context.Context, id uint) (*dto.UserDTO, error)
	GetUserByEmail(ctx context.Context, email, password string) (*dto.UserDTO, error)
	CreateUser(ctx context.Context, user *models.Users) (*dto.UserDetailDTO, error)
	UpdateUser(ctx context.Context, user *models.Users) (*dto.UserDetailDTO, error)
	UpdatePassword(ctx context.Context, user *models.Users) (*dto.UserDetailDTO, error)
	DeleteUser(ctx context.Context, id uint) error
}

type IMasterClassesService interface {
	GetAllMasterClasses(ctx context.Context, filterMC *filtering.MasterClassesFilter) ([]*dto.MasterClassDTO, error)
	GetMasterClassByID(ctx context.Context, id uint) (*dto.MasterClassDetailDTO, error)
	CreateMasterClass(ctx context.Context, masterClass *models.MasterClasses) (*dto.MasterClassDTO, error)
	UpdateUserCreateMC(ctx context.Context, masterClass *models.MasterClasses) (*dto.MasterClassDTO, error)
	UpdateMasterClass(ctx context.Context, updateMC *models.MasterClasses) (*dto.MasterClassDTO, error)
	DeleteMasterClass(ctx context.Context, authorID, masterClassID uint) error
}

type IBoardsService interface {
	GetAllBoards(ctx context.Context, filterBoard *filtering.BoardsFilter) ([]*dto.BoardDTO, error)
	GetBoardByID(ctx context.Context, id uint) (*dto.BoardDetailDTO, error)
	CreateBoard(ctx context.Context, board *models.Boards) (*dto.BoardDTO, error)
	UpdateBoard(ctx context.Context, updateBoard *models.Boards) (*dto.BoardDTO, error)
	DeleteBoard(ctx context.Context, userID, boardID uint) error
}
