package services

import (
	"context"
	"log"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/converters"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/dto"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/repositories"
)

type BoardsService struct {
	BoardsRepo repositories.IBoardsRepository
}

func (s *BoardsService) GetAllBoards(
	ctx context.Context, filterBoard *filtering.BoardsFilter) ([]*dto.BoardDTO, error) {
	boards, err := s.BoardsRepo.FindAll(ctx, filterBoard)
	if err != nil {
		return nil, err
	}

	if len(*boards) == 0 {
		log.Println("BoardsController.GetBoards master classes not found")
		return nil, mistakes.ErrNotFound
	}

	return converters.ConverterInListBoardsDTO(boards)
}

func (s *BoardsService) GetBoardByID(
	ctx context.Context, id uint) (*dto.BoardDetailDTO, error) {
	board, err := s.BoardsRepo.FindByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInBoardDetailDTO(board)
}

func (s *BoardsService) CreateBoard(
	ctx context.Context, userBoard *models.Boards) (*dto.BoardDTO, error) {
	err := s.BoardsRepo.Create(ctx, userBoard)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInNewBoardDTO(userBoard)
}

func (s *BoardsService) UpdateBoard(
	ctx context.Context, updateBoard *models.Boards) (*dto.BoardDTO, error) {
	updBoard, err := s.BoardsRepo.Update(ctx, updateBoard)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInNewBoardDTO(updBoard)
}

func (s *BoardsService) DeleteBoard(
	ctx context.Context, userID, boardID uint) error {
	return s.BoardsRepo.Delete(ctx, userID, boardID)
}

// Подтвердждаем, что BoardsService реализует интерфейс IBoardsService
var _ IBoardsService = (*BoardsService)(nil)
