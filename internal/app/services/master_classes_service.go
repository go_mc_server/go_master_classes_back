package services

import (
	"context"

	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/converters"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/dto"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/filtering"
	l "gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/mistakes"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/models"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/repositories"
)

type MasterClassesService struct {
	MasterClassRepo repositories.IMasterClassesRepository
}

func (s *MasterClassesService) GetAllMasterClasses(
	ctx context.Context, filterMC *filtering.MasterClassesFilter) ([]*dto.MasterClassDTO, error) {
	masterClasses, err := s.MasterClassRepo.FindAll(ctx, filterMC)
	if err != nil {
		return nil, err
	}

	if len(*masterClasses) == 0 {
		l.Warn("MasterClassController.GetMasterClasses master classes not found")
		return nil, mistakes.ErrNotFound
	}

	return converters.ConverterInListMasterClassesDTO(masterClasses)
}

func (s *MasterClassesService) GetMasterClassByID(
	ctx context.Context, id uint) (*dto.MasterClassDetailDTO, error) {
	masterClass, err := s.MasterClassRepo.FindByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInMasterClassDTO(masterClass)

}

func (s *MasterClassesService) CreateMasterClass(
	ctx context.Context, masterClass *models.MasterClasses) (*dto.MasterClassDTO, error) {

	err := s.MasterClassRepo.Create(ctx, masterClass)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInNewMasterClassDTO(masterClass)
}

func (s *MasterClassesService) UpdateUserCreateMC(
	ctx context.Context, masterClass *models.MasterClasses) (*dto.MasterClassDTO, error) {
	err := s.MasterClassRepo.UpdateUserCreateMC(ctx, masterClass)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInNewMasterClassDTO(masterClass)
}

func (s *MasterClassesService) UpdateMasterClass(
	ctx context.Context, updateMC *models.MasterClasses) (*dto.MasterClassDTO, error) {
	masterClass, err := s.MasterClassRepo.Update(ctx, updateMC)
	if err != nil {
		return nil, err
	}

	return converters.ConverterInNewMasterClassDTO(masterClass)
}

func (s *MasterClassesService) DeleteMasterClass(
	ctx context.Context, authorID, masterClassID uint) error {
	return s.MasterClassRepo.Delete(ctx, authorID, masterClassID)
}

// Подтвердждаем, что MasterClassesService реализует интерфейс IMasterClassesService
var _ IMasterClassesService = (*MasterClassesService)(nil)
