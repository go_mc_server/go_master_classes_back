package models

import "gorm.io/gorm"

type CurrentStatus string

const (
	Idea       CurrentStatus = "Idea"       //идея
	ToDo       CurrentStatus = "ToDo"       //сделать
	InProgress CurrentStatus = "InProgress" //в процессе
	Done       CurrentStatus = "Done"       // готово
	Archived   CurrentStatus = "Archived"   //архив
)

// Модель доски пользователя
type Boards struct {
	gorm.Model    `swaggerignore:"true"`
	ID            uint          `gorm:"primaryKey" json:"id" example:"1"`
	User          Users         `gorm:"foreignKey:UserID;references:ID;not null" json:"user,omitempty" swaggerignore:"true"`
	UserID        uint          `json:"user_id" example:"1"`
	MasterClass   MasterClasses `gorm:"foreignKey:MasterClassID;references:ID;not null" json:"master_class,omitempty" swaggerignore:"true"`
	MasterClassID uint          `json:"master_class_id" example:"1"`
	CurrentStatus CurrentStatus `gorm:"default:Idea" json:"current_status" example:"Idea"`
	Note          string        `gorm:"type:text" json:"note" example:"This is a note"`
}
