package models

import (
	"gorm.io/gorm"
)

type TypeNeedlework string

const (
	Crocheting          TypeNeedlework = "Crocheting"          //вязание крючком
	KnittingWithNeedles TypeNeedlework = "KnittingWithNeedles" //вязание спицами
	Macrame             TypeNeedlework = "Macrame"             //макраме
	Embroidery          TypeNeedlework = "Embroidery"          //вышивание
	Sewing              TypeNeedlework = "Sewing"              //шитьё
	Painting            TypeNeedlework = "Painting"            //рисование
	Felting             TypeNeedlework = "Felting"             //валяние
	Modeling            TypeNeedlework = "Modeling"            //лепка
)

// Модель мастер-классов
type MasterClasses struct {
	gorm.Model     `swaggerignore:"true"`
	ID             uint           `gorm:"primaryKey" json:"id" example:"1"`
	Author         Users          `gorm:"foreignKey:AuthorID;references:ID;not null" json:"author,omitempty" swaggerignore:"true"`
	AuthorID       uint           `json:"author_id" json:"author_id" example:"1"`
	TypeNeedlework TypeNeedlework `gorm:"not null" json:"type_needlework" example:"type_needlework"`
	Name           string         `gorm:"size:100;not null" json:"name" example:"name"`
	Description    string         `gorm:"size:500" json:"description" example:"description"`
	Materials      string         `json:"materials" example:"materials"`
	DetailInfo     string         `gorm:"type:text" json:"detail_info" example:"detail_info"`
	IsActive       bool           `gorm:"default:true" json:"is_active" example:"true"`
}
