// Определение структур данных (моделей) для пользователя и, возможно, других сущностей.
package models

import (
	"time"
)

// Допустимые значения для поля Role
type RoleType string

const (
	Admin      RoleType = "Admin"
	ActiveUser RoleType = "ActiveUser"
	Master     RoleType = "Master"
	Blocked    RoleType = "Blocked"
)

// Модель пользователя
type Users struct {
	ID         uint      `gorm:"primaryKey" json:"id" example:"1"`
	LastName   string    `gorm:"size:50;not null" json:"last_name" example:"Last Name"`
	FirstName  string    `gorm:"size:50;not null" json:"first_name" example:"First name"`
	MiddleName string    `gorm:"size:50" json:"middle_name" example:"Middle name"`
	Email      string    `gorm:"size:255;unique;not null" json:"email" example:"user.email@example.com"`
	Password   string    `gorm:"size:255;not null" json:"password" example:"password" swaggerignore:"true"`
	Role       RoleType  `gorm:"size:255;not null;default:ActiveUser" json:"role" example:"ActiveUser"`
	IsActive   bool      `gorm:"default:true" json:"is_active" example:"true"`
	CreatedAt  time.Time `gorm:"autoCreateTime" json:"created_at" example:"2023-01-01T00:00:00Z"`
	UpdatedAt  time.Time `gorm:"autoUpdateTime" json:"updated_at" example:"2023-01-01T00:00:00Z"`
}
