package pkg

import (
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/config"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/grpc_back/grpc_server"
	"gitlab.com/go_mc_server/go_master_classes_back/internal/app/logging"
)

func StartApplication() {
	logging.SetupLogger()
	config.InitENV()
	config.InitDB()
	grpc_server.RunGRPCServer()
}
